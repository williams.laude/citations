-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        wil
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-21 10:06
-- Created:       2022-02-21 10:06
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "auteur"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45) NOT NULL
);
CREATE TABLE "citation"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "citation" LONGTEXT NOT NULL,
  "auteur_id" INTEGER NOT NULL,
  CONSTRAINT "fk_citation_auteur"
    FOREIGN KEY("auteur_id")
    REFERENCES "auteur"("id")
);
CREATE INDEX "citation.fk_citation_auteur_idx" ON "citation" ("auteur_id");
COMMIT;
