import { Application } from "express";
import HomeController from "./controllers/HomeController";
import CitationController from "./controllers/CitationController";
import BackofficeController from "./controllers/BackofficeController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });
    
    app.get('/back', (req, res) =>{
        BackofficeController.back(req, res);
    })

    app.get('/Create-author',(req, res)=>{
        BackofficeController.showformAuteur(req, res)
    })

    app.post('/Create-author', (req, res)=>{
        BackofficeController.createAutheur(req, res)
    })

    app.get('/back/:id', (req, res)=>{
        BackofficeController.deleteAutheur(req, res)
    })

    app.get('/create-citation', (req, res)=>{
        BackofficeController.showFormCitation(req, res)
    })

    app.post('/create-citation', (req, res)=>{
            BackofficeController.updateCitation(req, res)
    })
    app.get('/back/:id',(req, res)=>{
        BackofficeController.deleteCitation(req, res)
    })

    app.get('/citation-update/:id',(req, res)=>{
        BackofficeController.showFormcitation(req, res)
    })

    app.post('/citation-update/:id',(req, res)=>{
        BackofficeController.formCitationUpdate(req, res)
    })

    app.get('/author-update/:id',(req, res)=>{
        BackofficeController.showFormUpdateAuthor(req, res)
    })

    app.post('/author-update/:id',(req, res)=>{
        BackofficeController.formAuthorUpdate(req, res)
    })

    app.get('/Citation/:id',(req, res)=>{
        CitationController.listCitation(req, res)
    })

    app.get('/auteur', (req, res)=>{
        CitationController.listAuteur(req, res)
    })

    app.get('/auteur-citation/:id', (req, res)=>{
        CitationController.listCitationByAuthor(req, res)
    })
}
