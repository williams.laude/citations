import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static index(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        let citation = db.prepare("SELECT * FROM citation WHERE id = (SELECT max(id) FROM citation)").get()
        let auteur = db.prepare("SELECT name FROM auteur WHERE id = ?").get(citation.auteur_id)

        res.render('pages/index', {
            title: 'Citation du jour',
            citation: citation,
            auteur: auteur
        });
    }

}