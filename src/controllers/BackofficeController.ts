import { Request, Response } from 'express-serve-static-core'

export default class BackofficeController {
    static back(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const user = db.prepare("SELECT * from auteur").all();
        const citation = db.prepare("SELECT * from citation").all()

        res.render('pages/back', {
            title: 'Hello Backoffice',
            user: user,
            citation: citation
        })
    }

    static showformAuteur(req: Request, res: Response) {
        res.render('pages/Create-author', {
            title: 'Ajouter une nouvelle auteur'
        })

    }

    static createAutheur(req: Request, res: Response) {
        const db = req.app.locals.db
        const user = db.prepare('INSERT INTO auteur ( "name") VALUES (?)').run(req.body.name);
        res.redirect('/back')
    }

    static deleteAutheur(req: Request, res: Response) {
        const db = req.app.locals.db
        const authdel = db.prepare('DELETE FROM auteur WHERE id = ?').run(req.params.id)
        res.redirect('/back')
    }

    static showFormCitation(req: Request, res: Response) {
        const db = req.app.locals.db
        const auteur = db.prepare('SELECT * FROM auteur').all()
        res.render('pages/create-citation', {
            title: 'Ajouter une nouvelle citation',
            auteur: auteur
        })
    }

    static updateCitation(req: Request, res: Response) {
        const db = req.app.locals.db
        const citation = db.prepare('INSERT INTO citation ("citation","auteur_id") VALUES (?,?)').run(req.body.citation, parseInt(req.body.auteur))
        res.redirect('/back')
    }

    static deleteCitation(req: Request, res: Response) {
        const db = req.app.locals.db
        const cit = db.prepare('DELETE FROM citation WHERE id = ?').run(req.params.id)
        res.redirect('/back')
    }

    static showFormUpdateAuthor(req: Request, res: Response) {
        const db = req.app.locals.db
        const user = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id)
        res.render(`pages/author-update`, {
            title: 'Infos autheur',
            user: user
        })
    }

    static showFormcitation(req: Request, res: Response) {
        const db = req.app.locals.db
        const cit = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id)
        const auteur = db.prepare('SELECT * FROM auteur WHERE id = ?').get(cit.auteur_id)

        res.render('pages/citation-update', {
            title: 'Modifier une citation',
            cit: cit,
            auteur: auteur
        })
    }

    static formCitationUpdate(req: Request, res: Response) {
        const db = req.app.locals.db
        db.prepare('UPDATE citation SET citation = ? WHERE id = ?').run(req.body.citation, req.params.id);
        res.redirect('/back')
    }

    static formAuthorUpdate(req: Request, res: Response) {
        const db = req.app.locals.db
        db.prepare('UPDATE auteur SET name = ? WHERE id = ?').run(req.body.name, req.params.id);
        res.redirect('/back')
    }
}