import { Request, Response } from 'express-serve-static-core'


export default class CitationController {
    static listCitation(req: Request, res: Response) {
        const db = req.app.locals.db
        let id_page = parseInt(req.params.id)
        const listperpage = 10
        const offset =(id_page - 1) * listperpage
        let count = db.prepare('SELECT count (*) from citation').all()
        let nbpages = Math.ceil(count[0]['count (*)']/10)
        const citation = db.prepare('SELECT * FROM citation INNER JOIN auteur ON citation.auteur_id = auteur.id LIMIT ?,?').all(offset,listperpage);

        res.render(`pages/Citation`, {
            title:'Liste des citations',
            citation: citation,
            id:id_page

        })
    }

    static listAuteur(req: Request, res: Response) {
        const db = req.app.locals.db;
        const auteur = db.prepare('SELECT * FROM auteur').all()

        res.render('pages/auteur', {
            title: 'Auteur',
            auteur: auteur
        })
    }

    static listCitationByAuthor(req: Request, res: Response) {
        const db = req.app.locals.db;
        const citation = db.prepare('SELECT * FROM citation WHERE auteur_id = ?').all(req.params.id)
        const auteur = db.prepare('SELECT name FROM auteur WHERE id = ?').get(req.params.id)

        res.render('pages/auteur-citation', {
            title: "citation de l'auteur",
            citation: citation,
            auteur: auteur
        })
    }
}